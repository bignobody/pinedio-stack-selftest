cmake_minimum_required(VERSION 3.19)

option(TARGET_V1_WITH_EXT_LCD "Build for PineDio STACK V1 with an external LCD" FALSE)
option(TARGET_V1 "Build for PineDio STACK V1" FALSE)
option(TARGET_V2 "Build for PineDio STACK V2" FALSE)

if(NOT ${TARGET_V1_WITH_EXT_LCD} AND NOT ${TARGET_V1} AND NOT ${TARGET_V2})
    message(FATAL_ERROR "Please specify one target to build!")
endif()

if(TARGET_V1_WITH_EXT_LCD)
    add_definitions(-DPINEDIO_HW_V1_EXT_LCD)
elseif(TARGET_V1)
    add_definitions(-DPINEDIO_HW_V1)
elseif(TARGET_V2)
    add_definitions(-DPINEDIO_HW_V2)
else()
    message(FATAL_ERROR "Please specify one target to build!")
endif()

add_definitions(-Dbl602_iot)
add_definitions(-Dbl602)
set(CHIP bl602)
set(BOARD bl602_iot)
set(SUPPORT_FLOAT "y")
set(SUPPORT_SHELL "n")

set(CMAKE_TOOLCHAIN_FILE cmake/riscv64-unknown-elf-gcc.cmake)
include(sdk/bl_mcu_sdk/drivers/${CHIP}_driver/cpu_flags.cmake)
include(sdk/bl_mcu_sdk/tools/cmake/compiler_flags.cmake)
include(sdk/bl_mcu_sdk/tools/cmake/tools.cmake)

project(pinedio-stack-selftest C ASM)
set(CMAKE_CXX_STANDARD 14)
set(LINKER_SCRIPT ${CMAKE_CURRENT_SOURCE_DIR}/sdk/bl_mcu_sdk/drivers/bl602_driver/bl602_flash.ld)

include_directories(sdk/bl_mcu_sdk/bsp/bsp_common/platform)
include_directories(sdk/bl_mcu_sdk/bsp/board/bl602)

add_compile_options(${GLOBAL_C_FLAGS})
add_compile_options($<$<COMPILE_LANGUAGE:C>:-std=c99>)
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-std=c++14>)
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-nostdlib>)
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>)
add_compile_options($<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>)

# lvgl
list(APPEND PROJECT_LVGL_INCLUDES
    "${CMAKE_CURRENT_SOURCE_DIR}/libs/lvgl/src"
    )
file(GLOB_RECURSE LVGL_SRC "${CMAKE_CURRENT_SOURCE_DIR}/libs/lvgl/*.c")
add_library(project-lvgl STATIC ${LVGL_SRC})
target_include_directories(project-lvgl SYSTEM PUBLIC ${PROJECT_LVGL_INCLUDES})
target_compile_options(project-lvgl PRIVATE
    $<$<AND:$<COMPILE_LANGUAGE:C>,$<CONFIG:DEBUG>>: ${COMMON_FLAGS} -Og -g3>
    $<$<AND:$<COMPILE_LANGUAGE:C>,$<CONFIG:RELEASE>>: ${COMMON_FLAGS} -Os>
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CONFIG:DEBUG>>: ${COMMON_FLAGS} -Og -g3 -fno-rtti>
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CONFIG:RELEASE>>: ${COMMON_FLAGS} -Os -fno-rtti>
    $<$<COMPILE_LANGUAGE:ASM>: -MP -MD -x assembler-with-cpp>
    )

add_executable(pinedio-stack-selftest-lcd
    sdk/bl_mcu_sdk/bsp/board/bl602/board.c
    sdk/bl_mcu_sdk/bsp/bsp_common/platform/bflb_platform.c
    sdk/bl_mcu_sdk/bsp/bsp_common/platform/syscalls.c

    main_lcd.c
    pinedio.h
    systeminfo.c
    systeminfo.h
    drivers/spi.c
    drivers/spi.h
    spiflash.c
    spiflash.h
    sx1262.c
    sx1262.h
    drivers/i2c.c
    drivers/i2c.h
    touchpanel.c
    touchpanel.h
    accelerometer.c
    accelerometer.h
    display.c
    display.h
    pushbutton.c
    pushbutton.h
    ui.c ui.h

    sdk/bl_mcu_sdk/examples/lvgl/demo/lv_demo_benchmark/lv_demo_benchmark.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/lv_demo_benchmark/lv_demo_benchmark.h
    sdk/bl_mcu_sdk/examples/lvgl/demo/lv_demo_widgets/lv_demo_widgets.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/lv_demo_widgets/lv_demo_widgets.h

    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/img_cogwheel_argb.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/img_cogwheel_alpha16.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/img_cogwheel_indexed16.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/img_cogwheel_chroma_keyed.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/img_cogwheel_rgb.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/lv_font_montserrat_12_compr_az.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/lv_font_montserrat_16_compr_az.c
    sdk/bl_mcu_sdk/examples/lvgl/demo/assets/lv_font_montserrat_28_compr_az.c

    drivers/cst816s.c
    drivers/cst816s.h
    drivers/st7789.c
    drivers/st7789.h
    ui/ui_main_menu.c
    ui/ui_main_menu.h
    ui/ui_system.c
    ui/ui_system.h
    ui/ui_pushbutton.c
    ui/ui_pushbutton.h
    ui/ui_touchpanel.c
    ui/ui_touchpanel.h
    ui/ui_accelerometer.c
    ui/ui_accelerometer.h
    ui/ui_display.c
    ui/ui_display.h
    ui/ui_sx1262.c
    ui/ui_sx1262.h
    battery.c
    battery.h
    ui/ui_battery.c
    ui/ui_battery.h)

add_executable(pinedio-stack-selftest-serial
    sdk/bl_mcu_sdk/bsp/board/bl602/board.c
    sdk/bl_mcu_sdk/bsp/bsp_common/platform/bflb_platform.c
    sdk/bl_mcu_sdk/bsp/bsp_common/platform/syscalls.c

    main_serial.c
    pinedio.h
    systeminfo.c
    systeminfo.h
    drivers/spi.c
    drivers/spi.h
    spiflash.c
    spiflash.h
    sx1262.c
    sx1262.h
    drivers/i2c.c
    drivers/i2c.h
    touchpanel.c
    touchpanel.h
    accelerometer.c
    accelerometer.h
    display.c
    display.h
    pushbutton.c
    pushbutton.h
    battery.c
    battery.h

    drivers/cst816s.c
    drivers/cst816s.h
    drivers/st7789.c
    drivers/st7789.h
    )

# Target LCD
target_link_libraries(pinedio-stack-selftest-lcd common bl602_driver freertos project-lvgl)
target_include_directories(pinedio-stack-selftest-lcd SYSTEM PUBLIC sdk/bl_mcu_sdk/bsp/bsp_common/platform)
target_include_directories(pinedio-stack-selftest-lcd SYSTEM PUBLIC sdk/bl_mcu_sdk/bsp/board/bl602)
target_compile_options(pinedio-stack-selftest-lcd PUBLIC
    $<$<AND:$<COMPILE_LANGUAGE:C>,$<CONFIG:DEBUG>>: ${GLOBAL_C_FLAGS} -Og -g3 -std=c99>
    $<$<AND:$<COMPILE_LANGUAGE:C>,$<CONFIG:RELEASE>>: ${GLOBAL_C_FLAGS} -Os -std=c99>
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CONFIG:DEBUG>>: ${GLOBAL_C_FLAGS} -Og -g3 -fno-rtti -nostdlib>
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CONFIG:RELEASE>>: ${GLOBAL_C_FLAGS} -Os -fno-rtti -nostdlib>
    #$<$<COMPILE_LANGUAGE:ASM>: -MP -MD -x assembler-with-cpp>
    )

set(MAP_FILE_LCD ${CMAKE_CURRENT_BINARY_DIR}/pinedio-stack-selftest-lcd.map)
target_link_options(pinedio-stack-selftest-lcd PRIVATE ${GLOBAL_LD_FLAGS})
set_target_properties(pinedio-stack-selftest-lcd PROPERTIES LINK_FLAGS "-T${LINKER_SCRIPT} -Wl,-Map=${MAP_FILE_LCD}")
set_target_properties(pinedio-stack-selftest-lcd PROPERTIES LINK_DEPENDS ${LINKER_SCRIPT})

add_dependencies(pinedio-stack-selftest-lcd bl602_driver)
target_link_libraries(pinedio-stack-selftest-lcd bl602_driver c)
target_link_libraries(pinedio-stack-selftest-lcd m)

set(BIN_FILE_LCD pinedio-stack-selftest-lcd.bin)
set(HEX_FILE_LCD pinedio-stack-selftest-lcd.hex)
add_custom_command(TARGET pinedio-stack-selftest-lcd POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:pinedio-stack-selftest-lcd> ${BIN_FILE_LCD}
    COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:pinedio-stack-selftest-lcd> ${HEX_FILE_LCD}
    COMMENT "Generate ${BIN_FILE_LCD}")

# Target SERIAL
target_link_libraries(pinedio-stack-selftest-serial common bl602_driver freertos )
target_include_directories(pinedio-stack-selftest-serial SYSTEM PUBLIC sdk/bl_mcu_sdk/bsp/bsp_common/platform)
target_include_directories(pinedio-stack-selftest-serial SYSTEM PUBLIC sdk/bl_mcu_sdk/bsp/board/bl602)
target_compile_options(pinedio-stack-selftest-serial PUBLIC
    $<$<AND:$<COMPILE_LANGUAGE:C>,$<CONFIG:DEBUG>>: ${GLOBAL_C_FLAGS} -Og -g3 -std=c99>
    $<$<AND:$<COMPILE_LANGUAGE:C>,$<CONFIG:RELEASE>>: ${GLOBAL_C_FLAGS} -Os -std=c99>
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CONFIG:DEBUG>>: ${GLOBAL_C_FLAGS} -Og -g3 -fno-rtti -nostdlib>
    $<$<AND:$<COMPILE_LANGUAGE:CXX>,$<CONFIG:RELEASE>>: ${GLOBAL_C_FLAGS} -Os -fno-rtti -nostdlib>
    )

set(MAP_FILE_SERIAL ${CMAKE_CURRENT_BINARY_DIR}/pinedio-stack-selftest-serial.map)
target_link_options(pinedio-stack-selftest-serial PRIVATE ${GLOBAL_LD_FLAGS})
set_target_properties(pinedio-stack-selftest-serial PROPERTIES LINK_FLAGS "-T${LINKER_SCRIPT} -Wl,-Map=${MAP_FILE_SERIAL}")
set_target_properties(pinedio-stack-selftest-serial PROPERTIES LINK_DEPENDS ${LINKER_SCRIPT})

add_dependencies(pinedio-stack-selftest-serial bl602_driver)
target_link_libraries(pinedio-stack-selftest-serial bl602_driver c)
target_link_libraries(pinedio-stack-selftest-serial m)

set(BIN_FILE_SERIAL pinedio-stack-selftest-serial.bin)
set(HEX_FILE_SERIAL pinedio-stack-selftest-serial.hex)
add_custom_command(TARGET pinedio-stack-selftest-serial POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:pinedio-stack-selftest-serial> ${BIN_FILE_SERIAL}
    COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:pinedio-stack-selftest-serial> ${HEX_FILE_SERIAL}
    COMMENT "Generate ${BIN_FILE_SERIAL}")

add_subdirectory(sdk/bl_mcu_sdk/common)
add_subdirectory(sdk/bl_mcu_sdk/drivers/bl602_driver)
add_subdirectory(sdk/bl_mcu_sdk/components/freertos)