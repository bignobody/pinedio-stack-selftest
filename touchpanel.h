#ifndef PINEDIO_STACK_SELFTEST_TOUCHPANEL_H
#define PINEDIO_STACK_SELFTEST_TOUCHPANEL_H

#include "pinedio.h"
Pinedio_ErrorCode_t touchpanel_check();

void touchpanel_on_top_left_tapped();
void touchpanel_on_top_right_tapped();
void touchpanel_on_bottom_left_tapped();

void touchpanel_set_color_ok();
void touchpanel_set_color_error();

Pinedio_ErrorCode_t touchpanel_get_test_result();

#endif // PINEDIO_STACK_SELFTEST_TOUCHPANEL_H
