//
// Created by jf on 1/15/22.
//

#ifndef PINEDIO_STACK_SELFTEST_I2C_H
#define PINEDIO_STACK_SELFTEST_I2C_H

#include "../pinedio.h"
void i2c_init();
void i2c_scan();

Pinedio_ErrorCode_t i2c_read(uint8_t deviceAddress, uint8_t registerAddress, uint8_t* data, uint16_t size);
Pinedio_ErrorCode_t i2c_write(uint8_t deviceAddress, uint8_t registerAddress, uint8_t* data, uint16_t size);

#endif // PINEDIO_STACK_SELFTEST_I2C_H
