#ifndef PINEDIO_STACK_SELFTEST_CST816S_H
#define PINEDIO_STACK_SELFTEST_CST816S_H

#include <stdint-gcc.h>
#include "../pinedio.h"


struct TouchInfos {
  int16_t x;
  int16_t y;
  uint8_t touching;
  uint8_t isValid;
};

void cst816s_init();
Pinedio_ErrorCode_t cst816s_get_touch_info(struct TouchInfos* info);
Pinedio_ErrorCode_t cst816s_get_chip_id(uint8_t* id);

#endif // PINEDIO_STACK_SELFTEST_CST816S_H
