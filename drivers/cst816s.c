#include "cst816s.h"
#include <bflb_platform.h>
#include "bl602_glb.h"
#include "../pinedio.h"
#include "hal_gpio.h"
#include "i2c.h"
#include "../ui.h"

#define CST816S_DEVICE_ADDRESS 0x15
#define CST816S_REG_CHIPID 0xA7
#define CST816S_REG_TOUCHDATA 0x0

struct TouchInfos lastTouchInfo;

void cst816s_on_interrupt() {
  cst816s_get_touch_info(&lastTouchInfo);
  if(lastTouchInfo.isValid) {
    ui_on_touch_event(lastTouchInfo.x, lastTouchInfo.y);
  }
}

void cst816s_init_pin() {
  GLB_GPIO_Type pins[1];
  pins[0] = (GLB_GPIO_Type)PINEDIO_PIN_TP_RESET;
  GLB_GPIO_Func_Init(
      GPIO_FUN_GPIO,  //  Configure the pins as GPIO
      pins,             //  Pins to be configured
      1
  );
  gpio_set_mode(PINEDIO_PIN_TP_RESET, GPIO_OUTPUT_PP_MODE);

  gpio_set_mode(GPIO_PIN_9, GPIO_SYNC_FALLING_TRIGER_INT_MODE);
  gpio_attach_irq(GPIO_PIN_9, cst816s_on_interrupt);
  gpio_irq_enable(GPIO_PIN_9, ENABLE);
}

void cst816s_reset() {
  gpio_write(PINEDIO_PIN_TP_RESET, 1);
  bflb_platform_delay_ms(50);
  gpio_write(PINEDIO_PIN_TP_RESET, 0);
  bflb_platform_delay_ms(5);
  gpio_write(PINEDIO_PIN_TP_RESET, 1);
  bflb_platform_delay_ms(50);
}

void cst816s_wakeup() {
  uint8_t dummy = 0;
  i2c_read(CST816S_DEVICE_ADDRESS, 0x15, &dummy, 1);
  bflb_platform_delay_ms(50);
  i2c_read(CST816S_DEVICE_ADDRESS, 0xa7, &dummy, 1);
  bflb_platform_delay_ms(50);
}

void cst816s_init() {
  cst816s_init_pin();
  cst816s_reset();
  cst816s_wakeup();
}

Pinedio_ErrorCode_t cst816s_get_touch_info(struct TouchInfos* info) {
  uint8_t touchData[7];

  if(i2c_read(CST816S_DEVICE_ADDRESS, CST816S_REG_TOUCHDATA, touchData, sizeof(touchData)) != PINEDIO_OK) {
    return PINEDIO_ERROR;
  }

  // This can only be 0 or 1
  uint8_t nbTouchPoints = touchData[2] & 0x0f;
  uint8_t xHigh = touchData[3] & 0x0f;
  uint8_t xLow = touchData[4];
  uint16_t x = (xHigh << 8) | xLow;
  uint8_t yHigh = touchData[5] & 0x0f;
  uint8_t yLow = touchData[6];
  uint16_t y = (yHigh << 8) | yLow;

  // Validity check
  if(x >= 240 || y >= 240) {
    info->isValid = 0;
  }

  info->x = x;
  info->y = y;
  info->touching = (nbTouchPoints > 0);
  info->isValid = 1;

  return PINEDIO_OK;
}

Pinedio_ErrorCode_t cst816s_get_chip_id(uint8_t* id) {
  return i2c_read(CST816S_DEVICE_ADDRESS, CST816S_REG_CHIPID, id, 1);
}

