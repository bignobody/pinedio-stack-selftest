#include <stdint.h>
#include <bflb_platform.h>
#include "spi.h"
#include "bl602_glb.h"
#include "bl602_spi.h"
#include "hal_gpio.h"
#include "../pinedio.h"

bool spi_need_cs = false;

void spi_init(bool need_cs, bool need_swap, bool need_miso, SPI_CLK_POLARITY_Type clkPolarity) {
  spi_need_cs = need_cs;
  uint8_t cs_pins[] = {PINEDIO_PIN_SX1262_CS, PINEDIO_PIN_FLASH_CS};
  GLB_GPIO_Func_Init(GPIO_FUN_SWGPIO, (GLB_GPIO_Type*)cs_pins, 2);
  gpio_set_mode(PINEDIO_PIN_SX1262_CS, GPIO_OUTPUT_PP_MODE);
  gpio_set_mode(PINEDIO_PIN_FLASH_CS, GPIO_OUTPUT_PP_MODE);
  gpio_write(PINEDIO_PIN_SX1262_CS, 1);
  gpio_write(PINEDIO_PIN_FLASH_CS, 1);

  uint8_t gpio_pins[] = { // TODO why doesn't HW CS work? Using manual CS for now
      PINEDIO_PIN_SPI_MOSI, /* MOSI */
      PINEDIO_PIN_SPI_CLK, /* SCK */
  };
  GLB_GPIO_Func_Init(GPIO_FUN_SPI, (GLB_GPIO_Type*)gpio_pins, 2);

  if(need_miso) {
    uint8_t miso_pins[] = {
        PINEDIO_PIN_SPI_MISO, /* MISO */
    };
    GLB_GPIO_Func_Init(GPIO_FUN_SPI, (GLB_GPIO_Type*)miso_pins, 1);
  }

  if(need_swap)
    GLB_Swap_SPI_0_MOSI_With_MISO(ENABLE);
  else
    GLB_Swap_SPI_0_MOSI_With_MISO(DISABLE);

  GLB_Set_SPI_0_ACT_MOD_Sel(GLB_SPI_PAD_ACT_AS_MASTER);

  SPI_IntMask((SPI_ID_Type)0, SPI_INT_ALL,MASK);

  SPI_Disable((SPI_ID_Type)0, SPI_WORK_MODE_MASTER);
  SPI_Disable((SPI_ID_Type)0, SPI_WORK_MODE_SLAVE);


  /* clock */
  /*1  --->  40 Mhz
   *2  --->  20 Mhz
   *5  --->  8  Mhz
   *6  --->  6.66 Mhz
   *10 --->  4 Mhz
   * */
  uint8_t clk_div = 10;
  GLB_Set_SPI_CLK(ENABLE,0);
  SPI_ClockCfg_Type clockcfg;
  clockcfg.startLen = clk_div;
  clockcfg.stopLen = clk_div;
  clockcfg.dataPhase0Len = clk_div;
  clockcfg.dataPhase1Len = clk_div;
  clockcfg.intervalLen = clk_div;
  SPI_ClockConfig(SPI_ID_0, &clockcfg);

  SPI_CFG_Type config;
  config.deglitchEnable = DISABLE;
  config.continuousEnable = ENABLE;
  config.byteSequence = SPI_BYTE_INVERSE_BYTE0_FIRST;
  config.bitSequence = SPI_BIT_INVERSE_MSB_FIRST;
  config.clkPhaseInv = SPI_CLK_PHASE_INVERSE_0;
  config.clkPolarity = clkPolarity;
  config.frameSize = SPI_FRAME_SIZE_8;

  SPI_Init(SPI_ID_0, &config);
  SPI_Enable(SPI_ID_0, SPI_WORK_MODE_MASTER);
}

void spi_transfer(GLB_GPIO_Type pinCS, uint8_t* cmd, size_t cmdSize, uint8_t* data, size_t dataSize) {
  if(spi_need_cs)
    gpio_write(pinCS, 0);

  SPI_SendRecv_8bits(SPI_ID_0, cmd, data, cmdSize, SPI_TIMEOUT_DISABLE);

  if(spi_need_cs)
    gpio_write(pinCS, 1);
}

void spi_write(GLB_GPIO_Type pinCS, uint8_t* data, size_t size) {
  if(spi_need_cs)
    gpio_write(pinCS, 0);

  SPI_Send_8bits(SPI_ID_0, data, size, SPI_TIMEOUT_DISABLE);

  if(spi_need_cs)
    gpio_write(pinCS, 1);
}