#ifndef PINEDIO_STACK_SELFTEST_ST7789_H
#define PINEDIO_STACK_SELFTEST_ST7789_H

#include <stddef.h>
#include <stdint.h>

void st7789_init();
void st7789_fill(uint16_t color);
void st7789_draw_buffer(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint8_t* buffer, size_t size);

#endif // PINEDIO_STACK_SELFTEST_ST7789_H
