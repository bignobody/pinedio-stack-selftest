#include <bflb_platform.h>
#include "st7789.h"
#include "bl602_gpio.h"
#include "../pinedio.h"
#include "bl602_glb.h"
#include "hal_gpio.h"
#include "spi.h"

#define PINEDIO_ST7789_CMD_SOFTWARE_RESET 0x01
#define PINEDIO_ST7789_CMD_SLEEP_OUT 0x11
#define PINEDIO_ST7789_CMD_COL_MOD 0x3A
#define PINEDIO_ST7789_CMD_MEMORY_DATA_ACCESS_CONTROL 0x36
#define PINEDIO_ST7789_CMD_COLUMN_ADDRESS_SET 0x2A
#define PINEDIO_ST7789_CMD_ROW_ADDRESS_SET 0x2B
#define PINEDIO_ST7789_CMD_ROW_INVERSION_ON 0x21
#define PINEDIO_ST7789_CMD_NORMAL_MODE_ON 0x13
#define PINEDIO_ST7789_CMD_DISPLAY_ON 0x29
#define PINEDIO_ST7789_CMD_WRITE_TO_RAM 0x2C

#define PINEDIO_ST7789_WIDTH ((uint16_t)240)
#define PINEDIO_ST7789_FULL_HEIGHT ((uint16_t)360)

void st7789_init_pins() {
  GLB_GPIO_Type pins[1];
  pins[0] = (GLB_GPIO_Type)PINEDIO_PIN_LCD_DC;
  GLB_GPIO_Func_Init(
      GPIO_FUN_SWGPIO,  //  Configure the pins as GPIO
      pins,             //  Pins to be configured
      1
  );
  gpio_set_mode(PINEDIO_PIN_LCD_DC, GPIO_OUTPUT_PP_MODE);
  gpio_write(PINEDIO_PIN_LCD_DC, 0);

#if defined(PINEDIO_LCD_NEED_RESET) && PINEDIO_LCD_NEED_RESET == true
  GLB_GPIO_Type pins_reset[1];
  pins_reset[0] = (GLB_GPIO_Type)PINEDIO_PIN_LCD_RESET;
  GLB_GPIO_Func_Init(
      GPIO_FUN_SWGPIO,  //  Configure the pins as GPIO
      pins_reset,             //  Pins to be configured
      1
  );
  gpio_set_mode(PINEDIO_PIN_LCD_RESET, GPIO_OUTPUT_PP_MODE);
  gpio_write(PINEDIO_PIN_LCD_RESET, 1);
#endif

#if defined(PINEDIO_LCD_NEED_CS) && PINEDIO_LCD_NEED_CS == true
  GLB_GPIO_Type pins_cs[1];
  pins_cs[0] = (GLB_GPIO_Type)PINEDIO_PIN_LCD_CS;
  GLB_GPIO_Func_Init(
      GPIO_FUN_SWGPIO,  //  Configure the pins as GPIO
      pins_cs,             //  Pins to be configured
      1
  );
  gpio_set_mode(PINEDIO_PIN_LCD_CS, GPIO_OUTPUT_PP_MODE);
  gpio_write(PINEDIO_PIN_LCD_CS, 1);

#endif

  bflb_platform_delay_ms(50);
}

void st7789_hw_reset() {
#if defined(PINEDIO_LCD_NEED_RESET) && PINEDIO_LCD_NEED_RESET == true
  gpio_write(PINEDIO_PIN_LCD_RESET, 0);
  bflb_platform_delay_ms(10);
  gpio_write(PINEDIO_PIN_LCD_RESET, 1);
  bflb_platform_delay_ms(100);
#endif
}

void st7789_send_command(uint8_t command) {
  uint8_t data = command;
  gpio_write(PINEDIO_PIN_LCD_DC, 0);
  spi_write(PINEDIO_PIN_LCD_CS, &data, 1);
}

void st7789_send_data(uint8_t data) {
  uint8_t buffer = data;
  gpio_write(PINEDIO_PIN_LCD_DC, 1);
  spi_write(PINEDIO_PIN_LCD_CS, &buffer, 1);
}

void st7789_init() {
  st7789_init_pins();
  spi_init(PINEDIO_LCD_NEED_CS, PINEDIO_LCD_NEED_SPI_SWAP, PINEDIO_LCD_NEED_MISO, SPI_CLK_POLARITY_HIGH);

  st7789_hw_reset();

  st7789_send_command(PINEDIO_ST7789_CMD_SOFTWARE_RESET);
  bflb_platform_delay_ms(150);

  st7789_send_command(PINEDIO_ST7789_CMD_SLEEP_OUT);

  st7789_send_command(PINEDIO_ST7789_CMD_COL_MOD);
  st7789_send_data(0x55);

  st7789_send_command(PINEDIO_ST7789_CMD_MEMORY_DATA_ACCESS_CONTROL);
  st7789_send_data(0x00);

  st7789_send_command(PINEDIO_ST7789_CMD_COLUMN_ADDRESS_SET);
  st7789_send_data(0x00);
  st7789_send_data(0x00);
  st7789_send_data(PINEDIO_ST7789_WIDTH >> 8u);
  st7789_send_data(PINEDIO_ST7789_WIDTH & 0xffu);

  st7789_send_command(PINEDIO_ST7789_CMD_ROW_ADDRESS_SET);
  st7789_send_data(0x00);
  st7789_send_data(0x00);
  st7789_send_data(PINEDIO_ST7789_FULL_HEIGHT >> 8u);
  st7789_send_data(PINEDIO_ST7789_FULL_HEIGHT & 0xffu);

  st7789_send_command(PINEDIO_ST7789_CMD_ROW_INVERSION_ON);
  bflb_platform_delay_ms(10);

  st7789_send_command(PINEDIO_ST7789_CMD_NORMAL_MODE_ON);
  bflb_platform_delay_ms(10);

  st7789_send_command(PINEDIO_ST7789_CMD_DISPLAY_ON);
}

void st7789_set_addr_window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
  st7789_send_command(PINEDIO_ST7789_CMD_COLUMN_ADDRESS_SET);
  st7789_send_data(x0 >> 8);
  st7789_send_data(x0 & 0xff);
  st7789_send_data(x1 >> 8);
  st7789_send_data(x1 & 0xff);

  st7789_send_command(PINEDIO_ST7789_CMD_ROW_ADDRESS_SET);
  st7789_send_data(y0 >> 8);
  st7789_send_data(y0 & 0xff);
  st7789_send_data(y1 >> 8);
  st7789_send_data(y1 & 0xff);

  st7789_send_command(PINEDIO_ST7789_CMD_WRITE_TO_RAM);
}


void st7789_fill(uint16_t color) {
  uint16_t buffer[240];
  for(int i = 0; i < 240; i++) {
    buffer[i] = color;
  }

  for(int i = 0; i < 240; i++) {
    st7789_set_addr_window(0, i, 239, i+1);
    gpio_write(PINEDIO_PIN_LCD_DC, 1);
    spi_write(PINEDIO_PIN_LCD_CS, (uint8_t *)buffer, 240*sizeof(uint16_t));
  }
}

void st7789_draw_buffer(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint8_t* buffer, size_t size) {
  st7789_set_addr_window(x0, y0, x1, y1);
  gpio_write(PINEDIO_PIN_LCD_DC, 1);
  spi_write(PINEDIO_PIN_LCD_CS, (uint8_t *)buffer, size);
}