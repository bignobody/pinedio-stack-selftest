#include <lv_core/lv_obj.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_label.h>
#include <lv_widgets/lv_btn.h>
#include "ui_sx1262.h"
#include "../sx1262.h"
#include "ui_main_menu.h"

lv_obj_t* ui_sx1262_btn_ok;

static void ui_sx1262_on_event(lv_obj_t * obj, lv_event_t event) {
  if(obj == ui_sx1262_btn_ok && event == LV_EVENT_PRESSED) {
    ui_main_menu_draw();
  }
}

void ui_sx1262_draw() {
  lv_obj_clean(lv_scr_act());

  lv_obj_t* label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label, "SX1262 (LoRa module)");
  lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
  lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
  lv_obj_set_width(label, 240);
  lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);

  lv_obj_t* label_result = lv_label_create(lv_scr_act(), NULL);
  if(sx1262_get_test_result() == PINEDIO_OK) {
    lv_label_set_text(label_result, LV_SYMBOL_OK);
  } else {
    lv_label_set_text(label_result, LV_SYMBOL_CLOSE);
  }
  lv_obj_set_style_local_text_color(label_result, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
  lv_label_set_align(label_result, LV_LABEL_ALIGN_CENTER);
  lv_obj_align(label_result, NULL, LV_ALIGN_CENTER, 0, 0);

  ui_sx1262_btn_ok = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_sx1262_btn_ok, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "OK");
  lv_obj_set_event_cb(ui_sx1262_btn_ok, ui_sx1262_on_event);
  lv_obj_align(ui_sx1262_btn_ok, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -50);
  lv_obj_set_height(ui_sx1262_btn_ok, 50);
}

void ui_sx1262_refresh(){

}