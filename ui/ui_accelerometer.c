#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_slider.h>
#include <stdio.h>
#include "ui_accelerometer.h"
#include "ui_main_menu.h"
#include "../accelerometer.h"

lv_obj_t* ui_accelerometer_slider_x;
lv_obj_t* ui_accelerometer_slider_y;
lv_obj_t* ui_accelerometer_slider_z;
lv_obj_t* ui_accelerometer_btn;
lv_obj_t* ui_accelerometer_label_result;

static void ui_accelerometer_on_event(lv_obj_t * obj, lv_event_t event) {
  if(obj == ui_accelerometer_btn && event == LV_EVENT_PRESSED) {
    ui_main_menu_draw();
  }
}

void ui_accelerometer_draw() {
  accelerometer_check();
  accelerometer_init();

  lv_obj_clean(lv_scr_act());

  ui_accelerometer_slider_x = lv_slider_create(lv_scr_act(), NULL);
  lv_obj_set_width(ui_accelerometer_slider_x, 200);
  lv_obj_set_height(ui_accelerometer_slider_x, 30);
  lv_obj_align(ui_accelerometer_slider_x, NULL, LV_ALIGN_IN_TOP_MID, 0, 10);
  lv_slider_set_range(ui_accelerometer_slider_x, -32768, 32767);
  lv_slider_set_value(ui_accelerometer_slider_x, 50, false);
  lv_obj_set_style_local_bg_color(ui_accelerometer_slider_x, LV_SLIDER_PART_BG, LV_STATE_DEFAULT, LV_COLOR_GRAY);

  ui_accelerometer_slider_y = lv_slider_create(lv_scr_act(), NULL);
  lv_obj_set_width(ui_accelerometer_slider_y, 200);
  lv_obj_set_height(ui_accelerometer_slider_y, 30);
  lv_obj_align(ui_accelerometer_slider_y, ui_accelerometer_slider_x, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);
  lv_slider_set_range(ui_accelerometer_slider_y, -32768, 32767);
  lv_slider_set_value(ui_accelerometer_slider_y, 50, false);
  lv_obj_set_style_local_bg_color(ui_accelerometer_slider_y, LV_SLIDER_PART_BG, LV_STATE_DEFAULT, LV_COLOR_GRAY);

  ui_accelerometer_slider_z = lv_slider_create(lv_scr_act(), NULL);
  lv_obj_set_width(ui_accelerometer_slider_z, 200);
  lv_obj_set_height(ui_accelerometer_slider_z, 30);
  lv_obj_align(ui_accelerometer_slider_z, ui_accelerometer_slider_y, LV_ALIGN_OUT_BOTTOM_MID, 0, 10);
  lv_slider_set_range(ui_accelerometer_slider_z, -32768, 32767);
  lv_slider_set_value(ui_accelerometer_slider_z, 50, false);
  lv_obj_set_style_local_bg_color(ui_accelerometer_slider_z, LV_SLIDER_PART_BG, LV_STATE_DEFAULT, LV_COLOR_GRAY);

  ui_accelerometer_btn = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_accelerometer_btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "OK");
  lv_obj_set_event_cb(ui_accelerometer_btn, ui_accelerometer_on_event);
  lv_obj_align(ui_accelerometer_btn, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -50);
  lv_obj_set_height(ui_accelerometer_btn, 50);

  ui_accelerometer_label_result = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(ui_accelerometer_label_result, "");
  lv_obj_set_style_local_text_color(ui_accelerometer_label_result, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
  lv_label_set_align(ui_accelerometer_label_result, LV_LABEL_ALIGN_CENTER);
  lv_obj_align(ui_accelerometer_label_result, ui_accelerometer_btn, LV_ALIGN_OUT_TOP_MID, 0, -20);
}

void ui_accelerometer_refresh() {
  struct accelerometer_raw_values_t values;
  accelerometer_get_raw_values(&values);

  lv_slider_set_value(ui_accelerometer_slider_x, values.x, false);
  lv_slider_set_value(ui_accelerometer_slider_y, values.y, false);
  lv_slider_set_value(ui_accelerometer_slider_z, values.z, false);

  if(accelerometer_get_test_result() == PINEDIO_OK) {
    lv_label_set_text(ui_accelerometer_label_result, LV_SYMBOL_OK);
  }
}
