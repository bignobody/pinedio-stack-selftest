#include <lv_core/lv_obj.h>
#include <lv_widgets/lv_cont.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_btn.h>
#include <stdio.h>
#include <bflb_platform.h>
#include "ui_main_menu.h"
#include "ui_system.h"
#include "../pinedio.h"
#include "../systeminfo.h"
#include "../pushbutton.h"
#include "ui_pushbutton.h"
#include "ui_touchpanel.h"
#include "../touchpanel.h"
#include "ui_accelerometer.h"
#include "ui_display.h"
#include "../display.h"
#include "../accelerometer.h"
#include "../sx1262.h"
#include "ui_sx1262.h"
#include "ui_battery.h"

lv_obj_t *bt_cpu;
lv_obj_t *bt_pushbutton;
lv_obj_t *bt_touch;
lv_obj_t *bt_motion;
lv_obj_t *bt_display;
lv_obj_t *bt_sx1262;
lv_obj_t *bt_battery;

enum UI_SCREEN {MAIN, CPU, PUSHBUTTON, TOUCH, ACCELEROMETER, DISPLAY, SX1262, BATTERY};
enum UI_SCREEN currentScreen = MAIN;

static void ui_on_btn_event(lv_obj_t * obj, lv_event_t event) {
  if(event != LV_EVENT_CLICKED) return;

  if(obj == bt_cpu) {
    currentScreen = CPU;
    ui_system_draw();
  } else if(obj == bt_pushbutton) {
    currentScreen = PUSHBUTTON;
    ui_pushbutton_draw();
  } else if(obj == bt_touch) {
    currentScreen = TOUCH;
    ui_touchpanel_draw();
  } else if(obj == bt_motion) {
    currentScreen = ACCELEROMETER;
    ui_accelerometer_draw();
  } else if(obj == bt_display) {
    currentScreen = DISPLAY;
    ui_display_draw();
  } else if(obj == bt_sx1262) {
    currentScreen = SX1262;
    ui_sx1262_draw();
  } else if(obj == bt_battery) {
    currentScreen = BATTERY;
    ui_battery_draw();
  }
}

static lv_color16_t ui_get_test_color(Pinedio_ErrorCode_t result) {
  switch(result){
  case PINEDIO_OK: return LV_COLOR_LIME;
  case PINEDIO_ERROR: return LV_COLOR_RED;
  case PINEDIO_PENDING: return LV_COLOR_ORANGE;
  default: return LV_COLOR_GRAY;
  }
}

void ui_main_menu_draw() {
  lv_obj_clean(lv_scr_act());
  currentScreen = MAIN;

  lv_obj_t *mainContainer;
  mainContainer = lv_cont_create(lv_scr_act(), NULL);
  lv_obj_set_auto_realign(mainContainer, true);
  lv_obj_align(mainContainer, NULL, LV_ALIGN_CENTER, 0, 0);
  lv_cont_set_fit(mainContainer, LV_FIT_NONE);
  lv_obj_set_size(mainContainer, 240, 240);
  lv_cont_set_layout(mainContainer, LV_LAYOUT_GRID);

  bt_cpu = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_cpu, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "SYS");
  lv_obj_set_style_local_value_color(bt_cpu, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(systeminfo_get_result()));
  lv_obj_set_event_cb(bt_cpu, ui_on_btn_event);
  lv_obj_set_size(bt_cpu, 110, 50);

  bt_touch = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_touch, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "Touch");
  lv_obj_set_style_local_value_color(bt_touch, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(touchpanel_get_test_result()));
  lv_obj_set_size(bt_touch, 110, 50);
  lv_obj_set_event_cb(bt_touch, ui_on_btn_event);

  bt_motion = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_motion, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "Motion");
  lv_obj_set_style_local_value_color(bt_motion, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(accelerometer_get_test_result()));
  lv_obj_set_size(bt_motion, 110, 50);
  lv_obj_set_event_cb(bt_motion, ui_on_btn_event);

  bt_sx1262 = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_sx1262, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "LoRa");
  lv_obj_set_style_local_value_color(bt_sx1262, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(sx1262_get_test_result(LV_COLOR_LIME)));
  lv_obj_set_size(bt_sx1262, 110, 50);
  lv_obj_set_event_cb(bt_sx1262, ui_on_btn_event);


  bt_display = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_display, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "Display");
  lv_obj_set_style_local_value_color(bt_display, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(display_get_test_result()));
  lv_obj_set_size(bt_display, 110, 50);
  lv_obj_set_event_cb(bt_display, ui_on_btn_event);

  bt_pushbutton = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_pushbutton, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "Button");
  lv_obj_set_style_local_value_color(bt_pushbutton, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(pushbutton_get_test_result()));
  lv_obj_set_size(bt_pushbutton, 110, 50);
  lv_obj_set_event_cb(bt_pushbutton, ui_on_btn_event);

  bt_battery = lv_btn_create(mainContainer, NULL);
  lv_obj_set_style_local_value_str(bt_battery, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "Battery");
  lv_obj_set_style_local_value_color(bt_battery, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, ui_get_test_color(pushbutton_get_test_result()));
  lv_obj_set_size(bt_battery, 110, 50);
  lv_obj_set_event_cb(bt_battery, ui_on_btn_event);
}

void ui_main_menu_refresh() {
  switch (currentScreen) {
  case MAIN:
    break;
  case CPU:
    break;
  case PUSHBUTTON:
    ui_pushbutton_refresh();
    break;
  case TOUCH:
    ui_touchpanel_refresh();
    break;
  case ACCELEROMETER:
    ui_accelerometer_refresh();
    break;
  case SX1262:
    ui_sx1262_refresh();
    break;
  case BATTERY:
    ui_battery_refresh();
    break;
  default:
    break;
  }
}

