#include <lv_core/lv_obj.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_label.h>
#include <stdio.h>
#include <lv_widgets/lv_btn.h>
#include "ui_system.h"
#include "../systeminfo.h"
#include "ui_main_menu.h"

lv_obj_t* ui_system_label;
lv_obj_t* ui_system_btn;
static void ui_system_on_event(lv_obj_t * obj, lv_event_t event) {
 if(obj == ui_system_btn && event == LV_EVENT_PRESSED) {
   ui_main_menu_draw();
 }
}

void ui_system_draw() {
  lv_obj_clean(lv_scr_act());
  systeminfo_get_system_info();

  ui_system_label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_long_mode(ui_system_label, LV_LABEL_LONG_BREAK);
  lv_label_set_text_fmt(ui_system_label, "CPU\n"
                           " - MCU : %s\n"
                           " - Ext : %s\n"
                           " - Pin : %s\n"
                           " - Mem : %s\n"
                           " - WiFi : \n   %s",
                        systeminfo_get_mcu_info(),
                        systeminfo_get_info_ext(),
                        systeminfo_get_pin_info(),
                        systeminfo_get_memory_info(),
                        systeminfo_get_wifi_mac());
  lv_obj_set_size(ui_system_label, 240,120);
  lv_obj_align(ui_system_label, NULL, LV_ALIGN_IN_TOP_MID, 5, 5);

  ui_system_btn = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_system_btn, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, "OK");
  lv_obj_set_event_cb(ui_system_btn, ui_system_on_event);
  lv_obj_align(ui_system_btn, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -40);
  lv_obj_set_height(ui_system_btn, 50);
}