#include <lv_core/lv_obj.h>
#include <lv_widgets/lv_btn.h>
#include <lv_core/lv_disp.h>
#include <lv_widgets/lv_label.h>
#include "ui_display.h"
#include "ui_main_menu.h"
#include "../display.h"

lv_obj_t* ui_display_btn_ok;
lv_obj_t* ui_display_btn_error;

static void ui_display_on_event(lv_obj_t * obj, lv_event_t event) {
  if(obj == ui_display_btn_ok && event == LV_EVENT_PRESSED) {
    display_set_color_ok();
    ui_main_menu_draw();
  } else if(obj == ui_display_btn_error && event == LV_EVENT_PRESSED) {
    display_set_color_error();
    ui_main_menu_draw();
  }
}

void ui_display_draw() {
  lv_obj_clean(lv_scr_act());

  lv_obj_t* label = lv_label_create(lv_scr_act(), NULL);
  lv_label_set_text(label, "Do you see green, red and blue squares ?");
  lv_label_set_long_mode(label, LV_LABEL_LONG_BREAK);
  lv_label_set_align(label, LV_LABEL_ALIGN_CENTER);
  lv_obj_set_width(label, 240);
  lv_obj_align(label, NULL, LV_ALIGN_IN_TOP_MID, 0, 5);

  lv_obj_t* label_green = lv_obj_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_bg_opa(label_green, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_obj_set_style_local_bg_color(label_green, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
  lv_obj_set_size(label_green, 50, 50);
  lv_obj_align(label_green, NULL, LV_ALIGN_CENTER, 0, 0);

  lv_obj_t* label_red = lv_obj_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_bg_opa(label_red, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_obj_set_style_local_bg_color(label_red, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_RED);
  lv_obj_set_size(label_red, 50, 50);
  lv_obj_align(label_red, label_green, LV_ALIGN_OUT_LEFT_MID, -0, 0);

  lv_obj_t* label_blue = lv_obj_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_bg_opa(label_blue, LV_OBJ_PART_MAIN, LV_STATE_DEFAULT, LV_OPA_COVER);
  lv_obj_set_style_local_bg_color(label_blue, LV_LABEL_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_BLUE);
  lv_obj_set_size(label_blue, 50, 50);
  lv_obj_align(label_blue, label_green, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

  ui_display_btn_ok = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_display_btn_ok, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_OK);
  lv_obj_set_style_local_value_color(ui_display_btn_ok, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_GREEN);
  lv_obj_set_event_cb(ui_display_btn_ok, ui_display_on_event);
  lv_obj_align(ui_display_btn_ok, NULL, LV_ALIGN_IN_BOTTOM_MID, -50, -50);
  lv_obj_set_height(ui_display_btn_ok, 50);

  ui_display_btn_error = lv_btn_create(lv_scr_act(), NULL);
  lv_obj_set_style_local_value_str(ui_display_btn_error, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_SYMBOL_CLOSE);
  lv_obj_set_style_local_value_color(ui_display_btn_error, LV_BTN_PART_MAIN, LV_STATE_DEFAULT, LV_COLOR_RED);
  lv_obj_set_event_cb(ui_display_btn_error, ui_display_on_event);
  lv_obj_align(ui_display_btn_error, NULL, LV_ALIGN_IN_BOTTOM_MID, 50, -50);
  lv_obj_set_height(ui_display_btn_error, 50);
}