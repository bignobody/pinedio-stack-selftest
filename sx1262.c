#include <stdint.h>
#include <bflb_platform.h>
#include "sx1262.h"
#include "drivers/spi.h"
#include "bl602_gpio.h"
#include "bl602_glb.h"
#include "hal_gpio.h"

Pinedio_ErrorCode_t sx1262_test_result = PINEDIO_PENDING;

void sx1262_display_buffer(const char* name, const uint8_t data[], size_t size) {
  printf("%s : ", name);
  for(int i = 0; i < size; i++) {
    printf("0x%02x ", data[i]);
  }
  printf("\r\n");
}

Pinedio_ErrorCode_t sx1262_check() {
  printf("SX1262 LoRa module\r\n");
  spi_init(true, true, true, SPI_CLK_POLARITY_LOW);
  GLB_GPIO_Type pins[1];
  pins[0] = (GLB_GPIO_Type)PINEDIO_PIN_SX1262_RESET;
  GLB_GPIO_Func_Init(
      GPIO_FUN_SWGPIO,  //  Configure the pins as GPIO
      pins,             //  Pins to be configured
      1
  );
  gpio_set_mode(PINEDIO_PIN_SX1262_RESET, GPIO_OUTPUT_PP_MODE);

  gpio_write(PINEDIO_PIN_SX1262_RESET, 0);
  bflb_platform_delay_ms(100);
  gpio_write(PINEDIO_PIN_SX1262_RESET, 1);

  bflb_platform_delay_ms(100);

  uint8_t cmdWriteBuffer[] = {0x0E, 0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0xAA, 0x55, 0x00, 0xFF};
  uint8_t responseWriteBuffer[11];

  spi_transfer(PINEDIO_PIN_SX1262_CS, cmdWriteBuffer, 11, responseWriteBuffer, 11);

  sx1262_display_buffer("\tWriteBuffer command", cmdWriteBuffer, 11);
  sx1262_display_buffer("\tWriteBuffer response", responseWriteBuffer, 11);

  uint8_t cmdReadBuffer[] = {0x1E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  uint8_t responseReadBuffer[12];
  spi_transfer(PINEDIO_PIN_SX1262_CS, cmdReadBuffer, 12, responseReadBuffer, 12);

  sx1262_display_buffer("\tReadBuffer command", cmdReadBuffer, 12);
  sx1262_display_buffer("\tReadBuffer response", responseReadBuffer, 12);

  printf("\tChecking data validity... ");
  size_t errorCount = 0;
  for(int i = 0; i < 9; i++) {
    if(responseReadBuffer[i + 3] != cmdWriteBuffer[i + 2]) {
      printf("\t[!] Invalid data at position %d. Received : 0x%2x - Expected: 0x%2x\r\n", i, responseReadBuffer[i + 3], cmdWriteBuffer[i + 2]);
      errorCount++;
    }
  }

  if(errorCount > 0) {
    printf("[ERROR]\r\n\r\n");
    sx1262_test_result = PINEDIO_ERROR;
  } else {
    printf("[OK]\r\n\r\n");
    sx1262_test_result = PINEDIO_OK;
  }
  return sx1262_test_result;
}

Pinedio_ErrorCode_t sx1262_get_test_result() {
  return sx1262_test_result;
}