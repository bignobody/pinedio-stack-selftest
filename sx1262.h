#ifndef PINEDIO_STACK_SELFTEST_SX1262_H
#define PINEDIO_STACK_SELFTEST_SX1262_H

#include "pinedio.h"
Pinedio_ErrorCode_t sx1262_check();
Pinedio_ErrorCode_t sx1262_get_test_result();

#endif // PINEDIO_STACK_SELFTEST_SX1262_H
