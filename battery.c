#include "battery.h"
#include "bl602_adc.h"
#include "bl602_gpio.h"
#include "bl602_glb.h"
#include "hal_gpio.h"

void battery_init() {
  gpio_set_mode(PINEDIO_PIN_CHARGING, GPIO_INPUT_PP_MODE);
}

Pinedio_ErrorCode_t battery_check() {
  printf("Battery\r\n");
  battery_init();
  GLB_Set_ADC_CLK(ENABLE, GLB_ADC_CLK_XCLK, 0);

  GLB_GPIO_Cfg_Type gpioCfg = {
      .gpioPin = GLB_GPIO_PIN_6,
      .gpioFun = (uint8_t)GPIO_FUN_ANALOG,
      .gpioMode = GPIO_MODE_ANALOG,
      .pullType = GPIO_PULL_NONE,
      .drive = 0,
      .smtCtrl = 1
  };

  GLB_GPIO_Init(&gpioCfg);

  ADC_Disable();
  ADC_Enable();
  ADC_Reset();

  ADC_CFG_Type adc_cfg = { 0 };
  adc_cfg.clkDiv = ADC_CLK_DIV_32;

  adc_cfg.vref = ADC_VREF_3P2V;
  adc_cfg.resWidth = ADC_DATA_WIDTH_16_WITH_256_AVERAGE;
  adc_cfg.inputMode = ADC_INPUT_SINGLE_END;

  adc_cfg.v18Sel = ADC_V18_SEL_1P82V;
  adc_cfg.v11Sel = ADC_V11_SEL_1P1V;
  adc_cfg.gain1 = ADC_PGA_GAIN_NONE;
  adc_cfg.gain2 = ADC_PGA_GAIN_NONE;
  adc_cfg.chopMode = ADC_CHOP_MOD_AZ_PGA_ON;
  adc_cfg.biasSel = ADC_BIAS_SEL_MAIN_BANDGAP;
  adc_cfg.vcm = ADC_PGA_VCM_1V;
  adc_cfg.offsetCalibEn = DISABLE;
  adc_cfg.offsetCalibVal = 0;

  ADC_Init(&adc_cfg);

  ADC_FIFO_Cfg_Type fifo_cfg = { 0 };
  fifo_cfg.fifoThreshold = 1;
  fifo_cfg.dmaEn = 0;
  ADC_FIFO_Cfg(&fifo_cfg);

  ADC_Channel_Config(ADC_CHAN5, ADC_CHAN_GND, true);

  arch_delay_ms(50);

  ADC_Start();
  arch_delay_ms(50);

  while (ADC_Get_FIFO_Count() < 10) {
  }

  uint32_t adc_fifo_val[10];
  for (uint32_t i = 0; i < 10; i++) {
    adc_fifo_val[i] = ADC_Read_FIFO();
  }
  ADC_Result_Type adc_parse_val[10];
  ADC_Parse_Result(adc_fifo_val, 10, adc_parse_val);

  //for(int i = 0; i<10; i++) {
    //int32_t v = adc_parse_val[i].volt * 1000.0f;
    //printf("ADC : %d - %d - %d - %d\r\n", adc_fifo_val[i], v, adc_parse_val[i].posChan, adc_parse_val[i].negChan);
  //}
  int32_t v = adc_parse_val[9].volt * 1000.0f;
  printf("\tBattery level %d mV\r\n", v);
  printf("\tBattery %s\r\n", (battery_is_charging()?"is charging" : "is not charging"));
  return PINEDIO_OK;
}

float battery_get_voltage() {
  GLB_Set_ADC_CLK(ENABLE, GLB_ADC_CLK_XCLK, 0);

  GLB_GPIO_Cfg_Type gpioCfg = {
      .gpioPin = GLB_GPIO_PIN_6,
      .gpioFun = (uint8_t)GPIO_FUN_ANALOG,
      .gpioMode = GPIO_MODE_ANALOG,
      .pullType = GPIO_PULL_NONE,
      .drive = 0,
      .smtCtrl = 1
  };

  GLB_GPIO_Init(&gpioCfg);

  ADC_Disable();
  ADC_Enable();
  ADC_Reset();

  ADC_CFG_Type adc_cfg = { 0 };
  adc_cfg.clkDiv = ADC_CLK_DIV_32;

  adc_cfg.vref = ADC_VREF_3P2V;
  adc_cfg.resWidth = ADC_DATA_WIDTH_16_WITH_256_AVERAGE;
  adc_cfg.inputMode = ADC_INPUT_SINGLE_END;

  adc_cfg.v18Sel = ADC_V18_SEL_1P82V;
  adc_cfg.v11Sel = ADC_V11_SEL_1P1V;
  adc_cfg.gain1 = ADC_PGA_GAIN_NONE;
  adc_cfg.gain2 = ADC_PGA_GAIN_NONE;
  adc_cfg.chopMode = ADC_CHOP_MOD_AZ_PGA_ON;
  adc_cfg.biasSel = ADC_BIAS_SEL_MAIN_BANDGAP;
  adc_cfg.vcm = ADC_PGA_VCM_1V;
  adc_cfg.offsetCalibEn = DISABLE;
  adc_cfg.offsetCalibVal = 0;

  ADC_Init(&adc_cfg);

  ADC_FIFO_Cfg_Type fifo_cfg = { 0 };
  fifo_cfg.fifoThreshold = 1;
  fifo_cfg.dmaEn = 0;
  ADC_FIFO_Cfg(&fifo_cfg);

  ADC_Channel_Config(ADC_CHAN5, ADC_CHAN_GND, true);

  arch_delay_ms(50);

  ADC_Start();
  arch_delay_ms(50);

  while (ADC_Get_FIFO_Count() < 10) {
  }

  uint32_t adc_fifo_val[10];
  for (uint32_t i = 0; i < 10; i++) {
    adc_fifo_val[i] = ADC_Read_FIFO();
  }
  ADC_Result_Type adc_parse_val[10];
  ADC_Parse_Result(adc_fifo_val, 10, adc_parse_val);

  for(int i = 0; i<10; i++) {
    int32_t v = adc_parse_val[i].volt * 1000.0f;
    printf("ADC : %d - %d - %d - %d\r\n", adc_fifo_val[i], v, adc_parse_val[i].posChan, adc_parse_val[i].negChan);
  }
  return adc_parse_val[9].volt;

}

bool battery_is_charging() {
  return gpio_read(PINEDIO_PIN_CHARGING) == 0;
}