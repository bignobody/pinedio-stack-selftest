#ifndef PINEDIO_STACK_SELFTEST_PUSHBUTTON_H
#define PINEDIO_STACK_SELFTEST_PUSHBUTTON_H

#include "pinedio.h"

void pushbutton_init();
Pinedio_ErrorCode_t pushbutton_get_test_result();
void pushbutton_enable();
void pushbutton_disable();

size_t pushbutton_get_nb_press();


#endif // PINEDIO_STACK_SELFTEST_PUSHBUTTON_H
