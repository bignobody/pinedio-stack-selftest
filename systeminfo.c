#include <bflb_platform.h>
#include "bl602_ef_ctrl.h"
#include "pinedio.h"

const char* systeminfo_device_info_extention_to_string(uint32_t devinfoextention) {
  switch(devinfoextention) {
  case 1: return "BL602C/BL604C (BLE wifi combo)";
  case 2: return "BL602L/BL604L (Light)";
  case 3:  return "BL602E/BL604E (Enhance)";
  default: return "Unknown";
  }
}

const char* systeminfo_memory_info_to_string(uint32_t memoryInfo) {
  switch(memoryInfo) {
  case 0: return "No memory";
  case 1: return "1MB flash";
  case 2: return "2MB flash";
  default: return "Unknown";
  }
}

const char* systeminfo_mcu_info_to_string(uint32_t mcuInfo) {
  switch(mcuInfo) {
  case 0: return "Wifi";
  case 1: return "MCU";
  default: return "Unknown";
  }
}

const char* systeminfo_pin_info_to_string(uint32_t pinInfo) {
  switch(pinInfo) {
  case 0: return "QFN32";
  case 1: return "QFN40";
  default: return "Unknown";
  }
}


const char*systeminfo_infoExt;
const char*systeminfo_memoryInfo;
const char*systeminfo_mcuInfo;
const char*systeminfo_pinInfo;
char systeminfo_chipId[127];
char systeminfo_wifiMac[127];
Pinedio_ErrorCode_t systeminfo_result = PINEDIO_PENDING;
uint8_t systeminfo_chipIdBuffer[8];
uint8_t systeminfo_wifiMacBuffer[8];

Pinedio_ErrorCode_t systeminfo_get_system_info(void) {
  Efuse_Device_Info_Type deviceInfo;
  EF_Ctrl_Read_Device_Info(&deviceInfo);
  EF_Ctrl_Read_Chip_ID(systeminfo_chipIdBuffer);
  EF_Ctrl_Read_MAC_Address(systeminfo_wifiMacBuffer);

  systeminfo_infoExt = systeminfo_device_info_extention_to_string(deviceInfo.rsvd_info);
  systeminfo_mcuInfo = systeminfo_mcu_info_to_string(deviceInfo.mcuInfo);
  systeminfo_pinInfo = systeminfo_pin_info_to_string(deviceInfo.pinInfo);
  systeminfo_memoryInfo = systeminfo_memory_info_to_string(deviceInfo.memoryInfo);
  snprintf(systeminfo_chipId, 127, "%d %d %d %d %d %d %d %d", systeminfo_chipIdBuffer[0],
           systeminfo_chipIdBuffer[1], systeminfo_chipIdBuffer[2],
           systeminfo_chipIdBuffer[3], systeminfo_chipIdBuffer[4],
           systeminfo_chipIdBuffer[5], systeminfo_chipIdBuffer[6],
           systeminfo_chipIdBuffer[7]);
  snprintf(systeminfo_wifiMac, 127, "%02x:%02x:%02x:%02x:%02x:%02x",
           systeminfo_wifiMacBuffer[0], systeminfo_wifiMacBuffer[1],
           systeminfo_wifiMacBuffer[2], systeminfo_wifiMacBuffer[3],
           systeminfo_wifiMacBuffer[4], systeminfo_wifiMacBuffer[5]);

  printf("Device Info : \r\n");
  printf("\t- Chip ID: %s\r\n", systeminfo_chipId);
  printf("\t- Customer ID : %d\r\n", deviceInfo.customerID);
  printf("\t- Info extension : %s\r\n", systeminfo_infoExt);
  printf("\t- Memory Info : %s\r\n", systeminfo_memoryInfo);
  printf("\t- Core Info : %d\r\n", deviceInfo.coreInfo);
  printf("\t- MCU Info : %s\r\n", systeminfo_mcuInfo);
  printf("\t- Pin Info : %s\r\n", systeminfo_pinInfo);
  printf("\t- WiFi MAC: %s\r\n", systeminfo_wifiMac);

  printf("\r\n\r\n");

  systeminfo_result = PINEDIO_OK;
  return systeminfo_result;
}

const char* systeminfo_get_info_ext() {
  return systeminfo_infoExt;
}

const char* systeminfo_get_mcu_info() {
  return systeminfo_mcuInfo;

}
const char* systeminfo_get_pin_info() {
  return systeminfo_pinInfo;
}

const char* systeminfo_get_memory_info() {
  return systeminfo_memoryInfo;
}

Pinedio_ErrorCode_t systeminfo_get_result() {
  return systeminfo_result;
}

const char* systeminfo_get_wifi_mac() {
  return systeminfo_wifiMac;
}
