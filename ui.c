#include "ui.h"
#include "drivers/cst816s.h"
#include "drivers/st7789.h"
#include "bl602_common.h"
#include "ui/ui_main_menu.h"

#include <stdio.h>
#include <lv_misc/lv_log.h>
#include <bflb_platform.h>
#include <lv_hal/lv_hal_disp.h>
#include <lv_core/lv_obj.h>
#include <lv_widgets/lv_btnmatrix.h>
#include <lv_core/lv_disp.h>

struct TouchEvent {
  int8_t newTouch;
  int16_t touchX;
  int16_t touchY;
};

struct TouchEvent ui_touch_event;
lv_disp_drv_t disp_drv;
lv_disp_buf_t disp_buf_2;
lv_color_t buf2_1[240 * 4];
lv_color_t buf2_2[240 * 4];

void lv_log_print_g_cb(lv_log_level_t level, const char *path, uint32_t line, const char *name, const char *str)
{
  switch (level) {
  case LV_LOG_LEVEL_ERROR:
    MSG("ERROR: ");
    break;

  case LV_LOG_LEVEL_WARN:
    MSG("WARNING: ");
    break;

  case LV_LOG_LEVEL_INFO:
    MSG("INFO: ");
    break;

  case LV_LOG_LEVEL_TRACE:
    MSG("TRACE: ");
    break;

  default:
    break;
  }

  MSG("%s- ", name);
  MSG(" %s\r\n,", str);
}

void disp_flush(lv_disp_drv_t* disp_drv, const lv_area_t* area, lv_color_t* color_p) {
  uint16_t width = (area->x2 - area->x1) + 1;
  uint16_t height = (area->y2 - area->y1) + 1;
  st7789_draw_buffer(area->x1, area->y1, area->x2, area->y2, (uint8_t *)color_p, width*height*2);

  lv_disp_flush_ready(disp_drv);
}

bool touchpad_read(lv_indev_drv_t* indev_drv, lv_indev_data_t* data) {
  __disable_irq();
  if(ui_touch_event.newTouch) {
    data->point.x = ui_touch_event.touchX;
    data->point.y = ui_touch_event.touchY;
    data->state = LV_INDEV_STATE_PR;
    ui_touch_event.newTouch = 0;
  }
  else {
    data->state = LV_INDEV_STATE_REL;
  }
  __enable_irq();
  return false;
}

void lv_port_disp_init() {
  lv_disp_buf_init(&disp_buf_2, buf2_1, buf2_2, 240 * 4);
  lv_disp_drv_init(&disp_drv);

  disp_drv.hor_res = 240;
  disp_drv.ver_res = 240;

  disp_drv.flush_cb = disp_flush;
  disp_drv.buffer = &disp_buf_2;

  lv_disp_drv_register(&disp_drv);
}

void lv_port_indev_init(){
  cst816s_init();

  lv_indev_drv_t indev_drv;

  lv_indev_drv_init(&indev_drv);
  indev_drv.type = LV_INDEV_TYPE_POINTER;
  indev_drv.read_cb = touchpad_read;
  lv_indev_drv_register(&indev_drv);
}



void ui_init() {
  printf("UI INIT\r\n");
  st7789_init();
  lv_log_register_print_cb(lv_log_print_g_cb);
  lv_init();
  lv_port_disp_init();
  lv_port_indev_init();
  //lv_demo_benchmark();
  ui_main_menu_draw();


  while (1) {
    ui_main_menu_refresh();
    lv_task_handler();
    bflb_platform_delay_ms(10);
  }
}

void ui_on_touch_event(int16_t x, int16_t y) {
  ui_touch_event.touchX = x;
  ui_touch_event.touchY = y;
  ui_touch_event.newTouch = 1;
}