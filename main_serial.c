#include "bflb_platform.h"

#include "systeminfo.h"
#include "sx1262.h"
#include "bl602_gpio.h"
#include "bl602_glb.h"
#include "hal_gpio.h"
#include "drivers/i2c.h"
#include "touchpanel.h"
#include "accelerometer.h"
#include "ui.h"
#include "battery.h"

int main(void) {
  bflb_platform_init(0);

  GLB_GPIO_Type pins[2];
  pins[0] = (GLB_GPIO_Type)PINEDIO_PIN_FLASH_CS;
  pins[1] = (GLB_GPIO_Type)PINEDIO_PIN_SX1262_CS;
  GLB_GPIO_Func_Init(
      GPIO_FUN_SWGPIO,  //  Configure the pins as GPIO
      pins,             //  Pins to be configured
      2
  );
  gpio_set_mode(PINEDIO_PIN_FLASH_CS, GPIO_OUTPUT_PP_MODE);
  gpio_set_mode(PINEDIO_PIN_SX1262_CS, GPIO_OUTPUT_PP_MODE);
  gpio_write(PINEDIO_PIN_FLASH_CS, 1);
  gpio_write(PINEDIO_PIN_SX1262_CS, 1);

  i2c_init();
  //i2c_scan();


  Pinedio_ErrorCode_t sysinfo_res = systeminfo_get_system_info();
  Pinedio_ErrorCode_t sx1262_res = sx1262_check();
  Pinedio_ErrorCode_t accelerometer_res = accelerometer_check();
  Pinedio_ErrorCode_t battery_res = battery_check();

  if(sysinfo_res == PINEDIO_OK && sx1262_res == PINEDIO_OK &&
      accelerometer_res == PINEDIO_OK && battery_res == PINEDIO_OK) {
    printf("Selftest result : OK !\r\n");
  } else {
    printf("Selftest result : ERROR !\r\n");
  }

  MSG("------------------------------------------------------------------\r\n");
  while (1) {
    bflb_platform_delay_ms(100);
  }
}
