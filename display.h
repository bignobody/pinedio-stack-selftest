#ifndef PINEDIO_STACK_SELFTEST_DISPLAY_H
#define PINEDIO_STACK_SELFTEST_DISPLAY_H

#include "pinedio.h"
Pinedio_ErrorCode_t display_check();

void display_draw_buffer(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint8_t* buffer, size_t size);

void display_set_color_ok();
void display_set_color_error();

Pinedio_ErrorCode_t display_get_test_result();

#endif // PINEDIO_STACK_SELFTEST_DISPLAY_H
