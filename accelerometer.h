#ifndef PINEDIO_STACK_SELFTEST_ACCELEROMETER_H
#define PINEDIO_STACK_SELFTEST_ACCELEROMETER_H

#include "pinedio.h"

struct accelerometer_raw_values_t {
  int16_t x;
  int16_t y;
  int16_t z;
};

Pinedio_ErrorCode_t accelerometer_check();

void accelerometer_get_raw_values(struct accelerometer_raw_values_t* values);
Pinedio_ErrorCode_t accelerometer_get_test_result();
void accelerometer_init();

#endif // PINEDIO_STACK_SELFTEST_ACCELEROMETER_H
