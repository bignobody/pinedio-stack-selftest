#include <bflb_platform.h>
#include "touchpanel.h"
#include "drivers/cst816s.h"

Pinedio_ErrorCode_t touchpanel_detect_result = PINEDIO_PENDING;
Pinedio_ErrorCode_t touchpanel_color_result = PINEDIO_PENDING;
bool touch_panel_is_top_left_tapped = false;
bool touch_panel_is_top_right_tapped = false;
bool touch_panel_is_bottom_left_tapped = false;

void touch_panel_check_touch_data() {
  struct TouchInfos info;
  for(int i = 0; i < 10; i++) {
    if(cst816s_get_touch_info(&info) == PINEDIO_OK) {
      printf("\tTouch data : %d, %d\r\n", info.x, info.y);
    } else {
      printf("\tError while reading touch data!\r\n");
    }
    bflb_platform_delay_ms(10);
  }
}

Pinedio_ErrorCode_t touchpanel_check() {
  printf("Touch Panel\r\n");
  cst816s_init();
  /* TODO : wake up from i2c does not seem to work, you'll need to manually wake it up by touching it */

  uint8_t chipId = 0;
  if(cst816s_get_chip_id(&chipId) == PINEDIO_OK) {
    printf("\tChipId : 0x%02x %s\r\n", chipId, (chipId == 0xb4) ? "[OK]":"[ERROR]");
    if(chipId == 0xb4) {
      touch_panel_check_touch_data();
      touchpanel_detect_result = PINEDIO_OK;
      return PINEDIO_OK;
    }
    touchpanel_detect_result = PINEDIO_ERROR;
    return PINEDIO_ERROR;
  } else {
    printf("\tError while reading ChipID!\r\n");
    touchpanel_detect_result = PINEDIO_ERROR;
    return PINEDIO_ERROR;
  }
}

void touchpanel_on_top_left_tapped() {
  touch_panel_is_top_left_tapped = true;
}

void touchpanel_on_top_right_tapped() {
  touch_panel_is_top_right_tapped = true;
}

void touchpanel_on_bottom_left_tapped() {
  touch_panel_is_bottom_left_tapped = true;
}

Pinedio_ErrorCode_t touchpanel_get_test_result() {
  if(touchpanel_detect_result == PINEDIO_ERROR)
    return touchpanel_detect_result;

  if(touch_panel_is_top_left_tapped == true &&
     touch_panel_is_top_right_tapped == true &&
     touch_panel_is_bottom_left_tapped == true) {
    return PINEDIO_OK;
  } else {
    return PINEDIO_PENDING;
  }
}

