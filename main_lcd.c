#include "bflb_platform.h"

#include "bl602_ef_ctrl.h"
#include "systeminfo.h"
#include "drivers/spi.h"
#include "spiflash.h"
#include "sx1262.h"
#include "bl602_gpio.h"
#include "bl602_glb.h"
#include "hal_gpio.h"
#include "drivers/i2c.h"
#include "touchpanel.h"
#include "accelerometer.h"
#include "display.h"
#include "pushbutton.h"
#include "ui.h"

int main(void) {
  bflb_platform_init(0);

  GLB_GPIO_Type pins[2];
  pins[0] = (GLB_GPIO_Type)PINEDIO_PIN_FLASH_CS;
  pins[1] = (GLB_GPIO_Type)PINEDIO_PIN_SX1262_CS;
  GLB_GPIO_Func_Init(
      GPIO_FUN_SWGPIO,  //  Configure the pins as GPIO
      pins,             //  Pins to be configured
      2
  );
  gpio_set_mode(PINEDIO_PIN_FLASH_CS, GPIO_OUTPUT_PP_MODE);
  gpio_set_mode(PINEDIO_PIN_SX1262_CS, GPIO_OUTPUT_PP_MODE);
  gpio_write(PINEDIO_PIN_FLASH_CS, 1);
  gpio_write(PINEDIO_PIN_SX1262_CS, 1);

  i2c_init();

  // Run SX1262 test before initializing the LCD as they
  // need different SPI configuration
  sx1262_check();

  ui_init();


  MSG("------------------------------------------------------------------\r\n");
  MSG("END\r\n");
  while (1) {
    bflb_platform_delay_ms(100);
  }
}
