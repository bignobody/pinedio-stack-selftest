#ifndef PINEDIO_STACK_SELFTEST_BATTERY_H
#define PINEDIO_STACK_SELFTEST_BATTERY_H

#include <stdbool.h>
#include "pinedio.h"

void battery_init();
Pinedio_ErrorCode_t battery_check();
float battery_get_voltage();
bool battery_is_charging();

#endif // PINEDIO_STACK_SELFTEST_BATTERY_H
