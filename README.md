# PineDio STACK selftest
Test firmware for the PineDio STACK.

# Build
Build this project like any CMake project:
- Clone the project from Git
- Initialize/update the Git submodules
- Create a build directory and invoke CMake with command line parameters to choose the target hardware
- Build
- Enjoy

```bash
git clone xxx
cd pinedio-stack_selftest
git submodule update --init
mkdir build && cd build
cmake -DTARGET_V2=1 ..
make -j
```

3 CMake options are available :
- `TARGET_V1_WITH_EXT_LCD`: Build for the V1 of the STACK with an external LCD display connected (the onboard one is disconnected). See below for connections.
- `TARGET_V1`: Build for the V1 of the STACK.
- `TARGET_V2`: Build for the V2 of the STACK.

# Hardware setup
The LCD display does not work on the V1 (prototype) version of the STACK. It's possible to test LCD integration by connectin another display to the board. Here is the pin mapping:

 * SPI CLK : GPIO11
 * SPI MOSI : GPIO0
 * LCD RESET : GPIO3
 * LCD D/C : GPIO17
 * LCD CS : N/A

Build with `-DTARGET_V1_WITH_EXT_LCD` to support this setup.